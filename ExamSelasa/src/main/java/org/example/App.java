package org.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        System.out.println( );

        int Y = 1998;
        int startDayOfMonth = 5;
        int spaces = startDayOfMonth;
        System.out.println(""+Y+"");

        String[] months = {
                "",
                "January", "February", "March",
                "April", "May", "June",
                "July", "August", "September",
                "October", "November", "December"
        };

        int[] days = {
                0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
        };

        for (int M = 1; M <= 12; M++) {

//            System.out.println(""+Y+"");
            System.out.println("               "+ months[M] + "          " );

            System.out.println("====================================");
            System.out.println("   Sun  Mon Tue   Wed Thu   Fri  Sat");

            spaces = (days[M-1] + spaces)%7;

            for (int i = 0; i < spaces; i++)
                System.out.print("     ");
            for (int i = 1; i <= days[M]; i++) {
                System.out.printf(" %3d ", i);
                if (((i + spaces) % 7 == 0) || (i == days[M])) System.out.println();
            }

            System.out.println();
            System.out.println("======================================");
            System.out.println();
        }
    }
}

